# adaptive-torrentvideo

A configurable package to apply bitwise adaptive loading to torrent videos on a web based javascript client. Based on webtorrent.

# install

```
npm install @konsumi/adaptive-torrentvideo
```
or
```
yarn add @konsumi/adaptive-torrentvideo
```

Then create a plugin.

```
import AdaptiveTorrentvideo from '@konsumi/adaptive-torrentvideo';
Vue.use(AdaptiveTorrentvideo)
```

# usage

You can use it as a directive in any component you like
by importing it and passing the your torrent which you want to stream:

```
<template>
  <video controls width="729" height="370" v-torrentvideo="{ 
    link: torrentLink,
    filepath: 'demo.mp4',
    quality: SD540p
  }"></video>
</template>

<script lang="ts">
import { Component, Vue } from 'nuxt-property-decorator';

@Component
export default class ClassApi extends Vue {
  torrentLink: string;

  data() {
    return {
      torrentLink: 'https://webtorrent.io/torrents/sintel.torrent'
    }
  }
}
</script>
```

## options

The plugin also takes some options on install.

```
Vue.use(AdaptiveTorrentvideo, { defaultTorrentLink: 'https://any-default.com/torrent' })
```

- defaultTorrentLink: can be added to immediately after initializing loading the given torrent link or magnet link

## license

[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2021 - Konsumi
