// const Torrent = require('@konsumi/adaptive-torrentvideo/node_modules/webtorrent/lib/torrent');
var ATTorrentHandler = /** @class */ (function () {
    function ATTorrentHandler(torrent, index) {
        this.torrent = torrent;
        this.index = index;
    }
    /**
     * finds a specific filepath within the
     * torrent
     *
     * @param filepath
     * @returns
     */
    ATTorrentHandler.prototype.findFilePath = function (filepath) {
        var _this = this;
        if (filepath === void 0) { filepath = null; }
        var fileToRender = this.torrent.files.find(function (file) {
            // if no filepath has been passed, return first file
            if (!filepath)
                return true;
            // only set rootDir if the file is in a directory
            var rootDir = /\//.test(file.path) ? (_this.torrent.name + '/') : '';
            // remove initial / if present
            var path = filepath.replace(/^\//, '');
            return file.path === rootDir + path;
        });
        return fileToRender;
    };
    /**
     * sets the index
     * @param index
     */
    ATTorrentHandler.prototype.setIndex = function (index) {
        this.index = index;
    };
    /**
     * returns the video file of the
     * torrent that comes closest to
     * the given bitrate (bit/s)
     */
    ATTorrentHandler.prototype.findVideoByClosestBitrate = function (bitrate) {
        var closest = this.findClosest(this.index.files, bitrate);
        return closest;
    };
    /**
     * returns the greatest possible
     * string that is in all array items
     * @param arr
     * @returns
     */
    ATTorrentHandler.prototype.findSubstringIntersection = function (arr) {
        // return a[0][e]?f(a,s+(r=a.some(x=>!x.includes(t),t=a[0].slice(s,++e))),r?w:t):w;
        var chars = arr[0].split(""), sub = "";
        for (var i = 0; i < chars.length; i++) {
            for (var j = 1; j < arr.length; j++) {
                if (arr[j].indexOf(chars[i]) == -1)
                    return sub;
            }
            sub += chars[i];
        }
        return sub;
    };
    /**
     * get all available torrent files
     */
    ATTorrentHandler.prototype.getTorrentFiles = function () {
        return this.torrent.files;
    };
    /**
     * find all video files of the same kind
     * by scanning a torrent
     * @deprecated
     */
    ATTorrentHandler.prototype.findSiblingVideos = function (basename) {
        if (basename === void 0) { basename = null; }
        // @todo also filter all videos only
        var filenames = this.getTorrentFiles().map(function (file) { return file.name; });
        if (!basename) {
            // try to find the greatest substring intersection in filenames automatically
            // const intersection = a =>(g=b=s=>a.every(x=>~x.indexOf(s))?b=b[s.length]?b:s:g(s.slice(0,-1,g(s.slice(1)))))(a[0]);
            basename = this.findSubstringIntersection(filenames);
        }
        var siblingFiles = this.torrent.files.find(function (file) { return file.path.indexOf(basename) !== -1; });
        return siblingFiles;
    };
    ATTorrentHandler.prototype.findClosest = function (array, needle) {
        return array.reduce(function (a, b) {
            return Math.abs(b.bitrate - needle) < Math.abs(a.bitrate - needle) ? b : a;
        });
    };
    return ATTorrentHandler;
}());
// export default ATTorrentHandler;
module.exports = ATTorrentHandler;
