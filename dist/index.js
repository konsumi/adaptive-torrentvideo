"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var WebTorrent = require("webtorrent");
var IdbkvChunkStore = require("idbkv-chunk-store");
var ATTorrent = require("./at-torrent");
var BITRATE_MONITOR_FREQ = 1000;
var mockIndex = {
    files: [
        {
            filename: "https://cocosico.miconoco.de/knee-720.webm.torrent",
            type: "webm",
            bitrate: 1100000,
            name: "knee intro low"
        },
        {
            filename: "https://cocosico.miconoco.de/knee-1440.webm.torrent",
            type: "webm",
            bitrate: 1400000,
            name: "knee intro mid"
        },
        {
            filename: "https://cocosico.miconoco.de/knee.mp4.torrent",
            type: "mp4",
            bitrate: 3000000,
            name: "knee intro high"
        },
    ]
};
// threshold for switching bitrate (in bits)
var THRESHOLD = 400000;
var AdaptiveTorrentvideo = {
    // module.exports = function AdaptiveTorrentvideo () {
    // return {
    // webtorrent client
    client: null,
    // passed plugin options
    options: null,
    // passed bindings
    bindings: null,
    // bound to element
    element: null,
    // current file
    currentFile: null,
    // loaded torrent
    torrent: null,
    // file handler service
    torrentHandler: null,
    currentVideoPointer: 0,
    initialLoad: false,
    bitrateMonitor: null,
    forceChangeQualityPromise: Promise,
    wasPlaying: false,
    mediaBuffer: null,
    /**
     * start render process with
     * given torrent
     */
    startRender: function () {
        var _this_1 = this;
        console.debug("start render torrent video: " + this.bindings.link);
        if (this.client.progress === 1 || this.torrent) {
            // there is already a loaded torrent - we need to remove it before beginning a new
            console.debug("remove loaded torrent to get new one");
            console.debug("remove: " + this.torrent.name);
            this.torrent.destroy();
            this.torrent = null;
        }
        // close existing interval for bitrate monitor
        if (this.bitrateMonitor) {
            clearInterval(this.bitrateMonitor);
        }
        if (this.bindings && this.bindings.link) {
            // first - check if we are already starting with lowest
            var lowestBitratOfSeries = this.getLowestVideo();
            if (lowestBitratOfSeries.filename !== this.bindings.link &&
                !this.initialLoad) {
                // start with lowest
                this.bindings.link = lowestBitratOfSeries.filename;
                this.initialLoad = true;
                console.debug("force video with lowest bitrate: " + this.bindings.link);
            }
            var link = this.bindings.link;
            var path = "";
            if (this.options && this.options.link) {
                link = this.options.link;
                path = this.bindings.filepath;
            }
            var pathRegex = /&path=([^&]+)/;
            if (pathRegex.test(link)) {
                path = pathRegex.exec(link)[1];
                link = link.replace(pathRegex, "");
            }
            this.torrent =
                this.client.get(link) ||
                    this.client.add(link, { store: IdbkvChunkStore });
            console.debug("add video to webtorrent client: " + this.bindings.link);
            // make sure metadata is available before path matching
            if (this.torrent.metadata) {
                this.renderFile();
            }
            else {
                this.torrent.once("ready", function () { return _this_1.renderFile(); });
            }
        }
    },
    /**
     * this is called when the
     * torrent is ready to be used
     */
    renderFile: function () {
        return __awaiter(this, void 0, void 0, function () {
            // handle on open source event for appending buffers that are coming from the promise above
            function sourceOpen(event) {
                return __awaiter(this, void 0, void 0, function () {
                    var sourceBuffer_1, _loop_1, _i, _a, chunk;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (!MediaSource.isTypeSupported(mimeCodec)) return [3 /*break*/, 5];
                                sourceBuffer_1 = mediaSource.addSourceBuffer(mimeCodec);
                                _loop_1 = function (chunk) {
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0: return [4 /*yield*/, new Promise(function (resolve) {
                                                    sourceBuffer_1.appendBuffer(chunk.mediaBuffer);
                                                    sourceBuffer_1.onupdateend = function (e) {
                                                        sourceBuffer_1.onupdateend = null;
                                                        sourceBuffer_1.timestampOffset += chunk.mediaDuration;
                                                        console.log(mediaSource.duration);
                                                        resolve(chunk);
                                                    };
                                                })];
                                            case 1:
                                                _a.sent();
                                                return [2 /*return*/];
                                        }
                                    });
                                };
                                _i = 0, _a = this.mediaBuffer;
                                _b.label = 1;
                            case 1:
                                if (!(_i < _a.length)) return [3 /*break*/, 4];
                                chunk = _a[_i];
                                return [5 /*yield**/, _loop_1(chunk)];
                            case 2:
                                _b.sent();
                                _b.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4:
                                mediaSource.endOfStream();
                                return [3 /*break*/, 6];
                            case 5:
                                console.warn(mimeCodec + " not supported");
                                _b.label = 6;
                            case 6: return [2 /*return*/];
                        }
                    });
                });
            }
            var _this, fileToRender, video, mediaSource, mimeCodec, bufferPromise, request, blobs, _a;
            var _this_1 = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _this = this;
                        this.torrentHandler = new ATTorrent(this.torrent, mockIndex);
                        this.bitrateMonitor = setInterval(function () {
                            _this.monitorBitrate();
                        }, BITRATE_MONITOR_FREQ);
                        fileToRender = this.torrentHandler.findFilePath(this.bindings.filepath);
                        if (!fileToRender) {
                            throw new Error("No file found matching this path: " + this.bindings.filepath);
                        }
                        this.currentFile = fileToRender;
                        console.debug("render video to element");
                        this.element.pause();
                        this.element.removeAttribute("src");
                        video = this.element;
                        mediaSource = new MediaSource();
                        mimeCodec = "video/webm; codecs=opus";
                        bufferPromise = new Promise(function (resolve) {
                            fileToRender.getBuffer(function (err, arrayBuffer) {
                                resolve(arrayBuffer);
                            });
                        });
                        request = bufferPromise;
                        return [4 /*yield*/, Promise.all([bufferPromise])];
                    case 1:
                        blobs = _b.sent();
                        // prepare blobs for mediasource by creating a promise with buffer and duration
                        _a = this;
                        return [4 /*yield*/, Promise.all(blobs.map(function (file) {
                                return new Promise(function (resolve) {
                                    var blobURL = URL.createObjectURL(new Blob([file]));
                                    video.onloadedmetadata = function (e) { return __awaiter(_this_1, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            resolve({
                                                mediaDuration: this.mediaBuffer.duration,
                                                mediaBuffer: file
                                            });
                                            return [2 /*return*/];
                                        });
                                    }); };
                                    video.src = blobURL;
                                });
                            }))];
                    case 2:
                        // prepare blobs for mediasource by creating a promise with buffer and duration
                        _a.mediaBuffer = _b.sent();
                        mediaSource.addEventListener("sourceopen", sourceOpen);
                        video.src = URL.createObjectURL(mediaSource);
                        return [2 /*return*/];
                }
            });
        });
    },
    /**
     * calculate bitrate
     * to see if we need can load
     * a higher quality
     */
    monitorBitrate: function () {
        if (!this.currentFile) {
            return false;
        }
        // keep track of current video position
        this.currentVideoPointer = this.element.currentTime;
        // video length in minutes
        var videoSizeBit = this.currentFile.length;
        // bitrate video
        // calc birate
        var videoBitrate = videoSizeBit / this.element.duration;
        var downloadBitSec = this.client.downloadSpeed;
        console.debug("bitrate monitor heartbeat, playing: " + this.torrent.name);
        console.debug("torrent loaded: " + (this.torrent.done ? "yes" : "no"));
        if (videoBitrate > 0 && downloadBitSec > 0 && this.client.progress !== 1) {
            var actualDiff = Math.abs(downloadBitSec - videoBitrate);
            var downloadRateDiff = actualDiff > THRESHOLD ? true : false;
            console.debug("montiored video bitrate: " +
                videoBitrate +
                " and download bitrate " +
                downloadBitSec);
            if (downloadRateDiff) {
                // @todo type the index
                var optimumVideo = this.torrentHandler.findVideoByClosestBitrate(downloadRateDiff);
                console.log("bitrate diff is: " +
                    actualDiff +
                    " need to switch to: " +
                    optimumVideo.filename);
                // download rate differs from the video rate - adapt video
                this.element.dispatchEvent(new window.CustomEvent("adaptive-torrentvideo-switch-resolution"));
                // set new binding
                console.log("start new rendering for: " + this.bindings.link);
                this.bindings.link = optimumVideo.filename;
                this.startRender();
            }
        }
    },
    getLowestVideo: function () {
        return mockIndex.files.sort(function (previous, current) {
            return previous.bitrate - current.bitrate;
        })[0];
    },
    loadSpecificVideo: function (torrentId) {
        // force given file
        this.bindings.link = torrentId;
        this.currentVideoPointer = this.element.currentTime;
        console.debug("clicked force specific quality torrent: " + torrentId);
        this.startRender();
    },
    /**
     * initialization
     */
    install: function (Vue, options) {
        var _this_1 = this;
        this.vue = Vue;
        Vue.WebTorrent = Vue.WebTorrent || new WebTorrent();
        if (options && options.defaultTorrentLink) {
            Vue.WebTorrent.add(options.defaultTorrentLink, {
                store: IdbkvChunkStore
            });
        }
        this.options = options;
        this.client = Vue.WebTorrent;
        Vue.mixin({
            data: function () {
                return {
                    $atClient: this.client
                };
            },
            created: function () {
                Vue.prototype.$atvideo = {
                    "nothing to see here": false
                };
            }
        });
        Vue.directive("torrentvideo", function (el, binding) {
            if (binding.value === binding.oldValue) {
                return;
            }
            _this_1.element = el;
            _this_1.bindings = binding.value;
            _this_1.client = Vue.WebTorrent;
            // register events to detect if video is playing
            _this_1.element.onplay = function () {
                _this_1.wasPlaying = true;
            };
            _this_1.element.onplaying = function () {
                _this_1.wasPlaying = true;
            };
            _this_1.element.onpause = function () {
                _this_1.wasPlaying = false;
            };
            // adaptive torrent video promises
            if (_this_1.bindings.forceQualitySwitch) {
                // wait for promise to come true
                _this_1.forceChangeQualityPromise = _this_1.bindings.forceQualitySwitch;
                _this_1.forceChangeQualityPromise.then(function (forceFile) {
                    // force given file
                    _this_1.bindings.link = forceFile;
                    _this_1.currentVideoPointer = _this_1.element.currentTime;
                    console.debug("clicked force specific quality torrent: " + forceFile);
                    _this_1.startRender();
                });
            }
            _this_1.startRender();
        });
    }
};
// }
exports["default"] = AdaptiveTorrentvideo;
// module.exports = AdaptiveTorrentvideo;
