"use strict";
exports.__esModule = true;
exports.ATResolutionEnum = {
    HD1080p: '1920X1080',
    HD720p: '1280X720',
    SD180p: '320X180',
    SD240p: '424X240',
    SD360p: '640X360',
    SD540p: '960X540'
};
