// const Torrent = require('@konsumi/adaptive-torrentvideo/node_modules/webtorrent/lib/torrent');

class ATTorrentHandler {
  constructor(private torrent, private index: any) {}

  /**
   * finds a specific filepath within the
   * torrent
   * 
   * @param filepath 
   * @returns 
   */
  public findFilePath(filepath: string = null): File {
    const fileToRender = this.torrent.files.find((file) => {
      // if no filepath has been passed, return first file
      if (!filepath) return true
      // only set rootDir if the file is in a directory
      const rootDir = /\//.test(file.path) ? (this.torrent.name + '/') : ''
      // remove initial / if present
      let path = filepath.replace(/^\//, '')
      return file.path === rootDir + path
    });
    return fileToRender;
  }

  /**
   * sets the index
   * @param index 
   */
  public setIndex(index: string){
    this.index = index;
  }

  /**
   * returns the video file of the
   * torrent that comes closest to
   * the given bitrate (bit/s)
   */
  public findVideoByClosestBitrate(bitrate: number): void {
    const closest = this.findClosest(this.index.files, bitrate);
    return closest;
  }

  /**
   * returns the greatest possible
   * string that is in all array items
   * @param arr
   * @returns 
   */
  private findSubstringIntersection(arr: string[]): string {
    // return a[0][e]?f(a,s+(r=a.some(x=>!x.includes(t),t=a[0].slice(s,++e))),r?w:t):w;
    let chars = arr[0].split(""), sub = "";
    for (let i=0;i<chars.length;i++) {
      for (let j=1;j<arr.length;j++) {
        if (arr[j].indexOf(chars[i])==-1) return sub;
      }
      sub+=chars[i];
    }
    return sub;
  }

  /**
   * get all available torrent files
   */
  private getTorrentFiles(): File[] {
    return this.torrent.files;
  }

  /**
   * find all video files of the same kind
   * by scanning a torrent
   * @deprecated
   */
   private findSiblingVideos(basename: string = null): File[] {
    // @todo also filter all videos only
    const filenames = this.getTorrentFiles().map((file: File) => file.name);
    if(!basename) {
      // try to find the greatest substring intersection in filenames automatically
      // const intersection = a =>(g=b=s=>a.every(x=>~x.indexOf(s))?b=b[s.length]?b:s:g(s.slice(0,-1,g(s.slice(1)))))(a[0]);
      basename = this.findSubstringIntersection(filenames);
    }
    const siblingFiles = this.torrent.files.find((file) => file.path.indexOf(basename) !== -1);
    return siblingFiles;
  }

  private findClosest(array, needle){
    return array.reduce((a, b) => {
      return Math.abs(b.bitrate - needle) < Math.abs(a.bitrate - needle) ? b : a;
    });
  }
}
// export default ATTorrentHandler;
module.exports = ATTorrentHandler