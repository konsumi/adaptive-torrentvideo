const WebTorrent = require("webtorrent");
const IdbkvChunkStore = require("idbkv-chunk-store");
const ATTorrent = require("./at-torrent");

const BITRATE_MONITOR_FREQ = 1000;

const mockIndex = {
  files: [
    {
      filename: "https://cocosico.miconoco.de/knee-720.webm.torrent",
      type: "webm",
      bitrate: 1100000,
      name: "knee intro low",
    },
    {
      filename: "https://cocosico.miconoco.de/knee-1440.webm.torrent",
      type: "webm",
      bitrate: 1400000,
      name: "knee intro mid",
    },
    {
      filename: "https://cocosico.miconoco.de/knee.mp4.torrent",
      type: "mp4",
      bitrate: 3000000,
      name: "knee intro high",
    },
  ],
};

// threshold for switching bitrate (in bits)
const THRESHOLD = 400000;

const AdaptiveTorrentvideo = {
  // module.exports = function AdaptiveTorrentvideo () {

  // return {
  // webtorrent client
  client: null,
  // passed plugin options
  options: null,
  // passed bindings
  bindings: null,
  // bound to element
  element: null,
  // current file
  currentFile: null,
  // loaded torrent
  torrent: null,
  // file handler service
  torrentHandler: null,

  currentVideoPointer: 0,

  initialLoad: false,
  bitrateMonitor: null,
  forceChangeQualityPromise: Promise,
  wasPlaying: false,

  mediaBuffer: null,

  /**
   * start render process with
   * given torrent
   */
  startRender() {
    console.debug("start render torrent video: " + this.bindings.link);

    if (this.client.progress === 1 || this.torrent) {
      // there is already a loaded torrent - we need to remove it before beginning a new
      console.debug("remove loaded torrent to get new one");
      console.debug("remove: " + this.torrent.name);
      this.torrent.destroy();
      this.torrent = null;
    }

    // close existing interval for bitrate monitor
    if (this.bitrateMonitor) {
      clearInterval(this.bitrateMonitor);
    }

    if (this.bindings && this.bindings.link) {
      // first - check if we are already starting with lowest
      const lowestBitratOfSeries = this.getLowestVideo();
      if (
        lowestBitratOfSeries.filename !== this.bindings.link &&
        !this.initialLoad
      ) {
        // start with lowest
        this.bindings.link = lowestBitratOfSeries.filename;
        this.initialLoad = true;
        console.debug("force video with lowest bitrate: " + this.bindings.link);
      }

      let link = this.bindings.link;
      let path = "";

      if (this.options && this.options.link) {
        link = this.options.link;
        path = this.bindings.filepath;
      }

      const pathRegex = /&path=([^&]+)/;
      if (pathRegex.test(link)) {
        path = pathRegex.exec(link)[1];
        link = link.replace(pathRegex, "");
      }
      this.torrent =
        this.client.get(link) ||
        this.client.add(link, { store: IdbkvChunkStore });
      console.debug("add video to webtorrent client: " + this.bindings.link);

      // make sure metadata is available before path matching
      if (this.torrent.metadata) {
        this.renderFile();
      } else {
        this.torrent.once("ready", () => this.renderFile());
      }
    }
  },

  /**
   * this is called when the
   * torrent is ready to be used
   */
  async renderFile() {
    var _this = this;
    this.torrentHandler = new ATTorrent(this.torrent, mockIndex);
    this.bitrateMonitor = setInterval(function () {
      _this.monitorBitrate();
    }, BITRATE_MONITOR_FREQ);
    // only render the first file found matching the path exactly
    var fileToRender = this.torrentHandler.findFilePath(this.bindings.filepath);
    if (!fileToRender) {
      throw new Error(
        "No file found matching this path: " + this.bindings.filepath
      );
    }
    this.currentFile = fileToRender;
    console.debug("render video to element");
    this.element.pause();
    this.element.removeAttribute("src");

    const video = this.element;
    const mediaSource = new MediaSource();
    // hardcode mime and codec for now - must be dynamic later
    const mimeCodec = "video/webm; codecs=opus";
    // const mimeCodec = "video/mp4; codecs=avc1.42E01E, mp4a.40.2";
    // mediaSource.mode = "sequence";

    // prepare promises to fetch buffer from webtorrent lib
    const bufferPromise = new Promise((resolve) => {
      fileToRender.getBuffer((err, arrayBuffer) => {
        resolve(arrayBuffer);
      });
    });
    const request = bufferPromise;

    // blobs can have multiple torrents later to be able to play on playlists
    const blobs = await Promise.all([bufferPromise]);

    // prepare blobs for mediasource by creating a promise with buffer and duration
    this.mediaBuffer = await Promise.all(
      blobs.map((file) => {
        return new Promise((resolve) => {
          let blobURL = URL.createObjectURL(new Blob([file as BlobPart]));
          video.onloadedmetadata = async (e) => {
            resolve({
              mediaDuration: this.mediaBuffer.duration,
              mediaBuffer: file,
            });
          };
          video.src = blobURL;
        });
      })
    );

    mediaSource.addEventListener("sourceopen", sourceOpen);
    video.src = URL.createObjectURL(mediaSource);

    // handle on open source event for appending buffers that are coming from the promise above
    async function sourceOpen(event) {
      if (MediaSource.isTypeSupported(mimeCodec)) {
        const sourceBuffer = mediaSource.addSourceBuffer(mimeCodec);
        for (let chunk of this.mediaBuffer) {
          await new Promise((resolve) => {
            sourceBuffer.appendBuffer(chunk.mediaBuffer);
            sourceBuffer.onupdateend = (e) => {
              sourceBuffer.onupdateend = null;
              sourceBuffer.timestampOffset += chunk.mediaDuration;
              console.log(mediaSource.duration);
              resolve(chunk);
            };
          });
        }
        mediaSource.endOfStream();
      } else {
        console.warn(mimeCodec + " not supported");
      }
    }
  },

  /**
   * calculate bitrate
   * to see if we need can load
   * a higher quality
   */
  monitorBitrate() {
    if (!this.currentFile) {
      return false;
    }
    // keep track of current video position
    this.currentVideoPointer = this.element.currentTime;

    // video length in minutes
    const videoSizeBit = this.currentFile.length;
    // bitrate video
    // calc birate
    const videoBitrate = videoSizeBit / this.element.duration;
    const downloadBitSec = this.client.downloadSpeed;

    console.debug("bitrate monitor heartbeat, playing: " + this.torrent.name);
    console.debug("torrent loaded: " + (this.torrent.done ? "yes" : "no"));

    if (videoBitrate > 0 && downloadBitSec > 0 && this.client.progress !== 1) {
      const actualDiff = Math.abs(downloadBitSec - videoBitrate);
      const downloadRateDiff = actualDiff > THRESHOLD ? true : false;

      console.debug(
        "montiored video bitrate: " +
          videoBitrate +
          " and download bitrate " +
          downloadBitSec
      );

      if (downloadRateDiff) {
        // @todo type the index
        const optimumVideo =
          this.torrentHandler.findVideoByClosestBitrate(downloadRateDiff);
        console.log(
          "bitrate diff is: " +
            actualDiff +
            " need to switch to: " +
            optimumVideo.filename
        );

        // download rate differs from the video rate - adapt video
        this.element.dispatchEvent(
          new window.CustomEvent("adaptive-torrentvideo-switch-resolution")
        );

        // set new binding
        console.log("start new rendering for: " + this.bindings.link);
        this.bindings.link = optimumVideo.filename;
        this.startRender();
      }
    }
  },

  getLowestVideo() {
    return mockIndex.files.sort((previous, current) => {
      return previous.bitrate - current.bitrate;
    })[0];
  },

  loadSpecificVideo(torrentId) {
    // force given file
    this.bindings.link = torrentId;
    this.currentVideoPointer = this.element.currentTime;
    console.debug("clicked force specific quality torrent: " + torrentId);
    this.startRender();
  },

  /**
   * initialization
   */
  install(Vue, options) {
    this.vue = Vue;
    Vue.WebTorrent = Vue.WebTorrent || new WebTorrent();

    if (options && options.defaultTorrentLink) {
      Vue.WebTorrent.add(options.defaultTorrentLink, {
        store: IdbkvChunkStore,
      });
    }
    this.options = options;
    this.client = Vue.WebTorrent;

    Vue.mixin({
      data() {
        return {
          $atClient: this.client,
        };
      },
      created() {
        Vue.prototype.$atvideo = {
          "nothing to see here": false,
        };
      },
    });

    Vue.directive("torrentvideo", (el, binding) => {
      if (binding.value === binding.oldValue) {
        return;
      }
      this.element = el;
      this.bindings = binding.value;
      this.client = Vue.WebTorrent;

      // register events to detect if video is playing
      this.element.onplay = () => {
        this.wasPlaying = true;
      };
      this.element.onplaying = () => {
        this.wasPlaying = true;
      };
      this.element.onpause = () => {
        this.wasPlaying = false;
      };

      // adaptive torrent video promises
      if (this.bindings.forceQualitySwitch) {
        // wait for promise to come true
        this.forceChangeQualityPromise = this.bindings.forceQualitySwitch;
        this.forceChangeQualityPromise.then((forceFile) => {
          // force given file
          this.bindings.link = forceFile;
          this.currentVideoPointer = this.element.currentTime;
          console.debug("clicked force specific quality torrent: " + forceFile);
          this.startRender();
        });
      }
      this.startRender();
    });
  },
};
// }
export default AdaptiveTorrentvideo;

// module.exports = AdaptiveTorrentvideo;
