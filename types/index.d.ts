import _Vue, { PluginObject, PluginFunction } from "vue";

export interface ATOptions {
  defaultTorrentLink: string,
  link: string
}

// definition for supported resolutions
export enum ATResolutionEnum {
  HD1080p = '1920X1080',
  HD720p = '1280X720',
  SD180p = '320X180',
  SD240p = '424X240',
  SD360p = '640X360',
  SD540p = '960X540'
}

export enum ATResolutionNamespace {
  // ATResolutionEnum elements are appended with lowdash to the filename
  Suffix = 'Suffix',
  // ATResolutionEnum elements are prepended with lowdash to the filename
  Prefix = 'Prefix',
  // ATResolutionEnum elements are mapped due to individual file names
  Mapping = 'Mapping'
}

export interface ATChunk {
  mediaDuration: number,
  mediaBuffer: Blob
}

export interface ATDirectiveParams {
  link: string,
  filepath: string,
  resolution: ATResolutionEnum,
  // define how the different videos are saved in the torrent
  resolutionNamespace: ATResolutionNamespace
}

export class AdaptiveTorrentvideo implements PluginObject<{}> {
  bindings: ATDirectiveParams;
  options: ATOptions;
  element: HTMLElement;
  mediaBuffer: ATChunk[];

  install: PluginFunction<{}>;
  static install(pVue: typeof _Vue, options?: ATOptions | undefined): void;
}
